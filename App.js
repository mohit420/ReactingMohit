import React, { Component } from 'react';
import { Alert, Platform, StyleSheet, Button, Text, TouchableHighlight, TouchableOpacity, TouchableNativeFeedback, TouchableWithoutFeedback, View, Linking } from 'react-native';
import FruitViewer from './FruitViewer';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types'; import {
  createStackNavigator,
} from 'react-navigation';

export class Touchables extends Component {
  
  _AppleButton() {
    const { navigate } = this.props.navigation;
    navigate('FruitViewer',{
      picurl1: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Honeycrisp.jpg/220px-Honeycrisp.jpg',
      title: 'Appy Fizz',
      linkurl: 'https://en.wikipedia.org/wiki/Apple',
      picurl2:'https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/95apple.jpeg/220px-95apple.jpeg',
      //opz: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/95apple.jpeg/220px-95apple.jpeg',
    })
  }

  _BananaButton() {
    const { navigate } = this.props.navigation;
    navigate('FruitViewer',{
      picurl1: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg',
      picurl2: 'https://i5.walmartimages.ca/images/Enlarge/580/6_r/875806_R.jpg',
      picurl3: 'http://www.adagio.com/images5/flavor_thumbnail/mango.jpg',            
      title: 'Banana',
      linkurl: 'https://en.wikipedia.org/wiki/Banana',
    });
  }


  static navigationOptions = ({navigation}) => {
    console.log(this.props)
    //onPress={navigation.state.params.RightHandler}
    return {
      title: 'Home',
      headerStyle: { backgroundColor: 'white' },
      headerTitleStyle: { color: 'blue' },
      headerRight: (
        <Icon
              onPress={navigation.getParam('_BananaButton')}
              name='home'
              size={25} //onPress={() => alert('This is a button!')}
              color="#0000ff" //onPress={() => alert('This is a button!')}
              style={{height:25,width:25}}
              />
            ),
          }
  }

  componentDidMount() {
    this.props.navigation.setParams({ _BananaButton: this._BananaButton });
  }

  constructor(props) {
    super(props);
    this._BananaButton = this._BananaButton.bind(this)
    this._AppleButton = this._AppleButton.bind(this)
  }  

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>    
        <TouchableHighlight onPress={this._BananaButton} underlayColor="white" >
          <View style={styles.button}>
            <Text style={styles.buttonText}>Banana</Text>
            </View>        
        </TouchableHighlight>   
        
        <TouchableOpacity onPress={this._AppleButton}>      
          <View style={styles.button}>
          <Icon
           name='arrow-right'
           size={25}
           color='#ff0000'
           style={{height:25,width:25}}/>
            <Text style={styles.buttonText}>Apple</Text>
          </View>
        </TouchableOpacity>
             
        <TouchableOpacity onPress={() => navigate('FruitViewer',{
            picurl1: 'http://files.uk2sitebuilder.com/uk2group263018/image/orange.jpg',
            title: 'Orange',
            linkurl: 'https://en.wikipedia.org/wiki/Orange',
            picurl2: 'https://www.stylecraze.com/wp-content/uploads/2013/05/Top-10-Benefits-Of-Orange-Peels-%E2%80%93-Why-They-Make-Your-Life-Better.jpg'
          })}>    
          <View style={styles.button}>
            <Text style={styles.buttonText}>Orange</Text>
          </View>
        </TouchableOpacity>


        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 90,
    alignItems: 'center',
      },
  button: {
    marginBottom: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#0000FF'
  },

  buttonText: {
    padding: 20,
    color: 'white'
  },


})


const App = createStackNavigator({
  Touchables: { screen: Touchables },
  FruitViewer: { screen: FruitViewer },
  
});

export default App;
