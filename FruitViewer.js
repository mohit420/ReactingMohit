
import React, { Component } from 'react';
import { AppRegistry, View, Text, StyleSheet, Image, TouchableHighlight, Linking } from 'react-native';

export default class FruitViewer extends Component {

    static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title', 'Fruit Viewer'),
      headerStyle: { backgroundColor: 'white' },
      headerTitleStyle: { color: 'blue' },
      };
  };

  render() {
    //this.navigationOptions.title = this.props.navigation.getParam('title');
    return (
      <View style={styles.container}>
      <Text style={styles.buttonText}>{this.props.navigation.getParam('title')}</Text>
        <Image source={{uri: this.props.navigation.getParam('picurl1')}} style={{width: 100, height: 110}}/>        
        <Image source={{uri: this.props.navigation.getParam('picurl2')}} style={{width: 100, height: 110}}/>
        <Image source={{uri: this.props.navigation.getParam('picurl3')}} style={{width: 100, height: 110}}/>
        <Text style={{color: 'blue'}} 
          onPress={() => Linking.openURL(this.props.navigation.getParam('linkurl')) }>
          {this.props.navigation.getParam('title')}
</Text> 
   
 </View>   
  
);
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    alignItems: 'center'
  }
})